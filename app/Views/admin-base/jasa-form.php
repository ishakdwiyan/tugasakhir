<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Form Input</h1>
    <hr>

    <?php 
    // menampilkan pesan error secara keseluruhan
    echo isset($validation)?$validation->listErrors():'';
    ?>

    <form action="<?php echo base_url(); ?>dashboard/data-jasa/simpan" method="post" enctype="multipart/form-data">     
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idnama">Nama</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="idnama" name="namajasa" 
                    value="<?php echo isset($namajasa)?$namajasa:
					    set_value('namajasa'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idslugJasa">Slug Jasa</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="idslugJasa" name="slugJasa" 
                    value="<?php echo isset($slugJasa)?$slugJasa:
					    set_value('slugJasa'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="hargaAnggota">Harga Anggota</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" id="hargaAnggota" name="hargaAnggota"
                    value="<?php echo isset($hargaAnggota)?$hargaAnggota:
					    set_value('hargaAnggota'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="hargaNonAnggota">Harga Non Anggota</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" id="hargaNonAnggota" name="hargaNonAnggota"
                    value="<?php echo isset($hargaNonAnggota)?$hargaNonAnggota:
					    set_value('hargaNonAnggota'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="hargaWnaCampur">Harga WNA Campuran</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" id="hargaWnaCampur" name="hargaWnaCampur"
                    value="<?php echo isset($hargaWnaCampur)?$hargaWnaCampur:
					    set_value('hargaWnaCampur'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="hargaWnaAsli">Harga WNA Asli</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" id="hargaWnaAsli" name="hargaWnaAsli"
                value="<?php echo isset($hargaWnaAsli)?$hargaWnaAsli:
					    set_value('hargaWnaAsli'); ?>">
            </div>
        </div>


        <div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idfoto">Foto</label>
    		<div class="col-sm-4">
    			<input type="file" class="form-control" id="idfoto" name="uploadfile[]">

				
    		</div>
    	</div>

        <?php 
		if(isset($foto))
			{
				?>
				
                    <div class="row mb-3">
                    <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-4">
                            <img src="<?php echo base_url();?>/assets/image/<?php echo $foto; ?>" alt="" width="60">

                            <input type="hidden" class="form-control" name="foto"
                            value="<?php echo isset($foto)?$foto:set_value('foto'); ?>">
                        </div>
                    </div>
				
				<?php
			}
		?>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idkategori">Kategori</label>
            <div class="col-sm-4">
                <select name="idkategori" id="idkategori" class="form-control">

                    <?php foreach($datakategori->getResult() as $itemjasakategori) 
                        { 
                            echo '<option value="'.$itemjasakategori->idKategori.'">'.$itemjasakategori->namaKategori.'</option>';
                        } 
                    ?>
                </select>
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idseotitle">SEO Title</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="idseotitle" name="seotitle"
                    value="<?php echo isset($seotitle)?$seotitle:
					    set_value('seotitle'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idseodesc">SEO Description</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="idseodesc" name="seodesc" 
                    value="<?php echo isset($seodesc)?$seodesc:
					    set_value('seodesc'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="idalt">ALT</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="idalt" name="alt" 
                    value="<?php echo isset($alt)?$alt:
					    set_value('alt'); ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-2 col-form-label" for="keterangan">Keterangan</label>
            <div class="col-sm-4">
                <textarea class="form-control" id="keterangan" name="keterangan" rows="7"><?php echo isset($keterangan)?$keterangan:set_value('keterangan'); ?>
                </textarea>
            </div>
        </div>
               
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-8">
                <input type="submit" class="btn btn-info" name="submit" value="Simpan">
            </div>
        </div> 

    </form>

</div>