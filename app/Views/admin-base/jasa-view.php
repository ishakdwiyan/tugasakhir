<div class="card shadow mb-4">
    <div class="card-header py-3">
        <?php echo anchor('/dashboard/data-jasa/tambah', 'Input Data', array('class'=>'btn btn-primary')); ?>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Harga Anggota</th>
                        <th>Harga Non-Anggota</th>
                        <th>Harga WNA Asli</th>
                        <th>Harga WNA Campur</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Harga Anggota</th>
                        <th>Harga Non-Anggota</th>
                        <th>Harga WNA Campur</th>
                        <th>Harga WNA Asli</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $no=1;
                    foreach ($dataJasa->getResult() as $itemjasa) 
                        { 
                            ?>
                                <tr>
                                    <td style="text-align: center;">
                                        <?php echo $no; ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->namajasa ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->namaKategori ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->hargaAnggota ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->hargaNonAnggota ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->hargaWnaCampur ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->hargaWnaAsli ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $itemjasa->keterangan ?>
                                    </td>
                                    <td>
                                        <?php echo anchor('dashboard/data-jasa/ubah/'.$itemjasa->idJasa, 'Ubah',array('class'=>'btn btn-warning')); ?>
                                        <?php echo anchor('dashboard/data-jasa/delete/'.$itemjasa->idJasa, 'Delete', array('class'=>'btn btn-danger')); ?>
                                    </td>
                                </tr>
                            <?php
                            $no++; 
                        } 
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
