    <!-- footer -->
    <div class="foter">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 mb-4">
            <h6>@Copyright 2023</h6>
            <div class="footer">
              <img src="assets/image/logo.png" alt="Logo MPUK Bali" />
              <img
                src="assets/image/logo2.jpg"
                alt="Logo MPUK Bali"
                class="border-radius"
              />
              <h6>
                +62 812-3677-1667 <br />info@yayasan-purna-bakti-mpuk-bali.com
                <br />Jl. By Pass Ngurah Rai Mubul, Kuta Selatan - Badung
              </h6>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <h4>Visit :</h4>
            <ul class="visit">
              <li>
                <a href="https://yayasan-purna-bakti-mpuk-bali.com/"
                  >Beranda<i data-feather="home"></i>
                </a>
              </li>
              <li>
                <a href="visi-misi.html"
                  >Visi & Misi<i data-feather="clipboard"></i>
                </a>
              </li>
              <li>
                <a href="kebijakan-umum.html"
                  >Kebijakan Umum<i data-feather="book"></i>
                </a>
              </li>
              <li>
                <a href="hubungi.html">Hubungi<i data-feather="phone"></i></a>
              </li>
            </ul>
          </div>

          <div class="col-lg-4">
            <h4>Follow Us :</h4>
            <ul class="follow">
              <li>
                <a href="#"><i data-feather="facebook"></i></a>
              </li>
              <li>
                <a href="#"><i data-feather="instagram"></i></a>
              </li>
              <li>
                <a href="#"><i data-feather="twitter"></i></a>
              </li>
              <li>
                <a href="https://www.youtube.com/@mpukprovinsibali9264/featured"
                  ><i data-feather="youtube"></i
                ></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- footer -->

    <!-- feather icons -->
    <script>
      feather.replace();
    </script>
    <!-- jquery -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-3.6.4.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/megamenu.js"></script>
    <!-- plugin -->

    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/owl-recent-works.js"></script>
    <script src="assets/js/script.js"></script>

    <script>
      jQuery(document).ready(function () {
        OwlCarousel.initOwlCarousel();
        OwlRecentWorks.initOwlRecentWorksV2();
      });
    </script>
  </body>
</html>
