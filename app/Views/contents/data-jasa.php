<div class="container-fluid">
    <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Jasa</th>
                    <th>Harga Anggota</th>
                    <th>Harga Non-Anggota</th>
                    <th>Harga WNA Campur</th>
                    <th>Harga WNA Asli</th>
                    <th>No Kategori</th>
                    <th>Keterangan</th>
                    <th>Foto</th>
                    <th>SEO Title</th>
                    <th>SEO Desc</th>
                    <th>Sequence</th>
                    <th>alt</th>
                </tr>   
                <?php $i = 1; ?>
                <?php foreach ($allDataJasa->getResult() as $itemjasa) : ?>
                    <tr>
                       
                        <td><?php echo $itemjasa->idJasa;?></td>
                        <td><?= $itemjasa->namajasa; ?></td>
                        <td><?= $itemjasa->hargaAnggota; ?></td>
                        <td><?= $itemjasa->hargaNonAnggota; ?></td>
                        <td><?= $itemjasa->hargaWnaCampur; ?></td>
                        <td><?= $itemjasa->hargaWnaAsli; ?></td>
                        <td><?= $itemjasa->idkategori; ?></td>
                        <td><?= $itemjasa->keterangan; ?></td>
                        <td><?= $itemjasa->foto; ?></td>
                        <td><?= $itemjasa->seotitle; ?></td>
                        <td><?= $itemjasa->seodesc; ?></td>
                        <td><?= $itemjasa->sequence; ?></td>
                        <td><?= $itemjasa->alt; ?></td>
                        
                        <td>
                            <button type="button" class="btn btn-primary mb-1">Ubah</button>
                            <button type="button" class="btn btn-danger">Hapus</button>
                        </td>
                    </tr>
                        
                    <?php $i++ ?>
                <?php endforeach; ?>    
    </table>
</div>


