<div class="container-fluid">
      <div class="row">
        <div class="col-12 p-0">
          <img src="assets/image/headline-hubungi.jpg" alt="Hubungi-Page-yayasan" class="w-100" />
        </div>
      </div>
    </div>

    <div class="info-bar">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mt-3 mb-3 box-info">
            <h2 class="hub"><i class="fa-solid fa-circle-info"></i>
               HUBUNGI KAMI :
            </h2>

            <div class="email">
              <h3>E-mail Yayasan Purna Bakti</h3>
              <a
                href="mailto:  info@yayasan-purna-bakti-mpuk-bali.com"
                title="Click to mail us"
                ><i data-feather="mail"></i>
                info@yayasan-purna-bakti-mpuk-bali.com</a
              >
            </div>

            <div class="call">
              <h3>Contact Yayasan Purna Bakti</h3>
              <a href="tel:+6281266736771667" title="Click to call us"
                ><i data-feather="phone"></i> +62 812-3677-1667 <br />
                <i data-feather="phone"></i> +62 878-6236-6824
              </a>
            </div>

            <div class="address">
              <h3>Alamat Yayasan Purna Bakti</h3>
              <p><i class="fa-solid fa-location-dot"></i>
                Jl. By Pass Ngurah Rai Mubul, Kuta Selatan - Badung
              </p>
            </div>
          </div>

          <div class="col-lg-6 mt-3 mb-3 opacity-75">
            <iframe class="w-100"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3942.9851591026954!2d115.19004307687284!3d-8.787463672661039!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd24364849f1229%3A0x7eabcce0da728b29!2sKrematorium%20Kerta%20Semadi%20Mumbul!5e0!3m2!1sid!2sid!4v1682056176521!5m2!1sid!2sid"
              height="450"
              style="border: 0"
              allowfullscreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade"
            ></iframe>
          </div>
        </div>
        <!-- end row -->

        <div class="row">
          <div class="col-12">
            <h2 style="font-size: 21px; font-weight: bold;"><i class="fa-solid fa-envelope"></i>
              Alamat E-mail :
            </h2>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-2"></div>
          <div class="col-8 mt-2 mail-pengurus">
            <h5>
              Ketua Pembina :
              <a href="mailto:  ketuapembina@yayasan-purna-bakti-mpuk-bali.com">
                <b>ketuapembina@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Anggota Pembina 1 :
              <a href="mailto:  ketuapembina@yayasan-purna-bakti-mpuk-bali.com">
                <b>anggotapembina1@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5 class="mb-4">
              Anggota Pembina 2 :
              <a href="mailto :  anggotapembina2@yayasan-purna-bakti-mpuk-bali.com">
                <b>anggotapembina2@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Ketua : 
              <a href="mailto :  ketua@yayasan-purna-bakti-mpuk-bali.com">
                <b>ketua@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Bendahara : 
              <a href="mailto:  bendahara@yayasan-purna-bakti-mpuk-bali.com">
                <b>bendahara@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5 class="mb-4">
              Sekretaris : 
              <a href="mailto:  sekretaris@yayasan-purna-bakti-mpuk-bali.com">
                <b>sekretaris@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Ketua Pengawas :
              <a href="mailto:  ketuapengawas@yayasan-purna-bakti-mpuk-bali.com">
                <b>ketuapengawas@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Anggota Pengawas 1 :
              <a href="mailto:  anggotapengawas1@yayasan-purna-bakti-mpuk-bali.com">
                <b>anggotapengawas1@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
            <h5>
              Anggota Pengawas 2 :
              <a href="mailto:  anggotapengawas2@yayasan-purna-bakti-mpuk-bali.com">
                <b>anggotapengawas2@yayasan-purna-bakti-mpuk-bali.com</b>
              </a>
            </h5>
          </div>
          <div class="col-2"></div>
        </div>
        <!-- end container -->
      </div>
    </div>
