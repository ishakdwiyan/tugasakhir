<div class="container-fluid p-0">
      <div class="headline-pelayanan">
        <img src="assets/image/headline-pelayanan.jpg" alt="Daftar-pelayana-page" class="w-100" />
      </div>
    </div>

    <!-- content -->

    <div class="container">
      <div class="row mt-5">
        <div class="col-12">
          <h2 style="font-size: 21px; font-weight: bold;"><i class="fa-solid fa-list" style="color: #000;"></i>  
            Daftar Pelayanan :
          </h2>
        </div>
        <div class="col-12 pelayanan-headline">
          <h1>
            YAYASAN PURNA BAKTI ADALAH YAYASAN YANG MEJALANKAN TAMAN PEMAKAMAN
            UNTUK UMAT KRISTEN <br />
            LAYANAN YANG ADA DI YAYASAN INI ANTARA LAIN :
          </h1>
        </div>
      </div>
      <div class="row pelayanan-penjelasan mb-5">
        <div class="col-md-6">
          <h3 style="font-size: 16px;">
            <a href="rumahpenghiburan.html">1. Rumah Penghiburan</a>
          </h3>
          <img src="assets/image/1-2.webp" alt="" class="w-100" />
          <h5 class="mt-2 mb-3" style="font-size: 14px;">
            Tempat yang disediakan oleh Yayasan Untuk pesemayaman jenazah
            sebelum dikuburkan.
          </h5>
        </div>
        <div class="col-md-6">
          <h3 style="font-size: 16px;">
            <a href="pemakaman.html">2. Pemakaman</a>
          </h3>
          <img src="assets/image/4-2.webp" alt="" class="w-100" />
          <h5 class="mt-2 mb-3" style="font-size: 14px;">
            Selain itu, Yayasan juga menyediakan taman pemakaman bagi umat
            kristen.
          </h5>
        </div>

        <div class="col-md-6">
          <h3 style="font-size: 16px;">
            <i class="fa-solid fa-truck-medical"></i>
             <a href="ambulans.html">3. Ambulans</a>
          </h3>
          <img src="assets/image/ambulan.webp" alt="" class="w-100" />
          <h5 class="mt-2 mb-3" style="font-size: 14px;">
            Yayasan juga meyediakan fasilitas berupa mobil Ambulans untuk
            menjemput mendiang.
          </h5>
        </div>
        <div class="col-md-6">
          <h3 style="font-size: 16px;">
            <a href="peti-jenazah">4. Peti Jenazah</a>
          </h3>
          <img src="assets/image/peti.jpg" alt="" class="w-100" />
          <h5 class="mt-2 mb-3" style="font-size: 14px;">Dan, Fasilitas lainnya seperti peti jenazah dimiliki.</h5>
        </div>

        <div class="col-md-6">
          <h3 style="font-size: 16px;">
            <a href="krematorium.html">5. Krematorium</a>
          </h3>
          <img src="assets/image/krematorium.jpg" alt="" class="w-100" />
          <h5 class="mt-2 " style="font-size: 14px;">
            Serta, Yayasan juga menyediakan fasilitas tempat untuk meng-kremasi
            jenazah.
          </h5>
        </div>
      </div>
    </div>

    <!-- content -->
