  <!-- Content -->

  <div class="container-fluid">
    <div class="row">
      <div class="col-12 p-0">
          <img
            src="assets/image/batu-nama.png"
            alt="FOTO TEMPAT LEMBAGA YAYASAN KRISTEN BALI"
            class="w-100"
          />
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12 mt-3">
        <h1 style="font-size: 20px; margin-bottom: 12px; margin-left: 50px; padding: 10px; border-radius: 4px; color: #fff; background-color: #145121; display: inline-block;">
          Lembaga Yayasan Umat Kristen Bali</h1>
        <h4 style="font-size: 17px;">
          Yayasan Purna Bakti MPUK Bali adalah badan hukum yang mempunyai
          maksud dan tujuan di bidang kemanusiaan dengan menjalankan Taman
          Pemakaman untuk umat Kristen yang ada di Bali.
          <br>
          Kami juga menyediakan Jasa Pelayanan terkhusus bagi umat Kristen, antara lain: 
          <br>
          - Pemakaman
          <br>
          - Rumah Duka
          <br>
          - Ambulan
          <br>
        </h4>
      </div>
    </div>

        <div class="row img-key mb-5">

        <?php 
          foreach ($allDataJasa->getResult() as $itemjasa)
          {       
            ?>
                <div class="col-md-4 mb-5">
                  <img src="<?php echo base_url();?>assets/image/<?php echo $itemjasa->foto?> " alt="Foto Fasilitas Rumah Penghiburan" class="w-100"/>
                  <p style="font-size: 17px; padding-top: 10px; display: block; color: black; text-decoration: none;">
                  <?php echo $itemjasa->namajasa; ?> </p>
                </div>
            <?php
          }
        ?>        

        <div class="col-md-12">
          <?php ?>
            <a href="data-product" style="font-size: 17px; padding-top: 10px; display: block; color: black;">
                Info Selengkapnya</a>
        </div> -->
        <p>*klik untuk memesan jasa pelayanan</p>
        </div>
    </div>
  


  <!-- <div class="container">
    <div class="row mb-5">
      <div class="col-12">
          <?php if (isset($_SESSION["login"])) : ?>
            <a href="logout.php" style="padding:10px; border-radius:12px; background-color:orange; color: white; text-decoration: none;">Logout</a>
          <?php endif; ?>
      </div>
    </div>
  </div> -->
  

  <!-- content -->

  <div class="container ">
    <div class="row" style="margin-bottom: 21px;">
      <div class="col-12 ">
        <div class="headline">
          <h2>
            <i class="fa-solid fa-handshake-simple"></i> 
            Mitra
          </h2>
        </div>
        <div class="owl-clients-v1">
          <div class="item">
            <img src="assets/image/logo-gkpb.png" alt="logo gkpb">
          </div>
          <div class="item">
            <img src="assets/image/logo-gbi.png" alt="logo gbi">
          </div>
          <div class="item">
            <img src="assets/image/logo-hkbp.png" alt="logo hkpb">
          </div>
          <div class="item">
            <img src="assets/image/logo-rock.png" alt="logo gbi rock">
          </div>
          <div class="item">
            <img src="assets/image/logo-gpib.png" alt="logo gpib">
          </div>
          <div class="item">
            <img src="assets/image/logo-gsja.png" alt="logo gsja">
          </div>
          <div class="item">
            <img src="assets/image/logo-gpdi.png" alt="logo gspdi">
          </div>
          <div class="item">
            <img src="assets/image/logo-gkii.png" alt="logo gkii">
          </div>
          <div class="item">
            <img src="assets/image/logo-gpt.png" alt="logo gpt">
          </div>
          <div class="item">
            <img src="assets/image/logo-gbis.png" alt="logo gbis">
          </div>
          <div class="item">
            <img src="assets/image/logo-gkpi.png" alt="logo gkpi">
          </div>
          <div class="item">
            <img src="assets/image/logo-pglii.png" alt="logo pglii Bali">
          </div>
          <div class="item">
            <img src="assets/image/logo-gpis.png" alt="logo gpis">
          </div>
          <div class="item">
            <img src="assets/image/logo-pgi.png" alt=";ogo pgi bali">
          </div>
          <div class="item">
            <img src="assets/image/logo-gab.png" alt="logo gab">
          </div>
          <div class="item">
            <img src="assets/image/logo-ggbi.png" alt="logo ggbi">
          </div>
          <div class="item">
            <img src="assets/image/logo-gppi.png" alt="logo gppi">
          </div>
          <div class="item">
            <img src="assets/image/logo-abbalove.png" alt="logo abbalove">
          </div>
          <div class="item">
            <img src="assets/image/logo-hok-imtong.png" alt="logo hok im tong">
          </div>
          <div class="item">
            <img src="assets/image/logo-gkn.png" alt="logo gkn">
          </div>
          <div class="item">
            <img src="assets/image/logo-hki.png" alt="logo hki">
          </div>
          <div class="item">
            <img src="assets/image/logo-gkps.png" alt="logo gkps">
          </div>
          <div class="item">
            <img src="assets/image/logo-gkkk.png" alt="logo gkkk">
          </div>
        </div>
        <!-- End Owl Clients v1 -->
      </div>
      <!--/container-->
    </div>
  </div>