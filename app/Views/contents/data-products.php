<div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="assets/image/4-2.webp" alt="Foto Fasilitas Pemakaman" class="w-100"/>
                <p style="font-size: 18px; padding-top: 10px;">Pemakaman Tersedia dengan 2 Paket yang berbeda.</p>
            </div>    
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/4-2.webp" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Pemakaman Dewasa</h5>
                    <a href="#" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/4-2.webp" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Pemakaman bayi</h5>
                    <a href="#" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <img src="assets/image/1-3.webp" alt="Foto Fasilitas Pemakaman" class="w-100"/>
                <p style="font-size: 18px; padding-top: 10px;">Rumah Duka Tersedia dengan 2 Paket yang berbeda.</p>
            </div>    
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/4-2.webp" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Rumah Duka Standar</h5>
                    <a href="#" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/4-2.webp" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Rumah Duka VIP</h5>
                    <a href="#" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <img src="assets/image/ambulans.png" alt="Foto Fasilitas Pemakaman" class="w-100"/>
                <p style="font-size: 18px; padding-top: 10px;">Jasa Ambulan Tersedia dengan 2 Paket yang berbeda.</p>
            </div>    
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/ambulan-1.png" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Area Badung-Denpasar</h5>
                    <a href="product.html" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card shadow-lg" style="width: 18rem;">
                    <img src="assets/image/ambulan-2.png" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title">Area Gianyar-Klungkung-Tabanan</h5>
                    <a href="#" class="btn btn-success">Info Detail</a>
                    </div>
                </div>
            </div>
        </div>
    </div>