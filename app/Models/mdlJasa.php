<?php
namespace App\Models;
use CodeIgniter\Model;

class mdlJasa extends Model
{
    protected $tbl="tbljasa";
    protected $primary="idJasa";

    protected $builder;
    protected $db;

    function __Construct()
    {
        $this->db= \Config\Database::connect();
        $this->builder=$this->db->table($this->tbl);
    }
    function getAlldatajoincat()
    {
        $this->builder->select("*");
        $this->builder->join('tblkategori', 'tblkategori.idKategori=tbljasa.idkategori');
        return $this->builder->get();
    }
    function getAlldata()
    {
        return $this->builder->get();
    }
    function DataBy($arrkriteria)
    {
        $this->builder->where($arrkriteria);
        return $this->builder->get();
    }
    function save_data($arrSave)
	{
		if($arrSave['idJasa']>0)
		{
			$this->builder->where('idJasa', $arrSave['idJasa']);
			$this->builder->update($arrSave);
			//update .. where idJasa=nilai kode
			
			return $arrSave['idJasa'];
		}
		else
		{
		
		// Perintah memasukkan nilai ke tabel
		 $this->builder->insert($arrSave);

		// Perintah untuk menangkap ID nilai yang baru dimasukkan
        return $this->db->insertID();
		}
	}
}