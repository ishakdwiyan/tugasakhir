<?php
namespace App\Models;
use CodeIgniter\Model;

class mdlkategori extends Model
{
    protected $tbl="tblkategori";
    protected $primary="idKategori";

    protected $builder;
    protected $db;

    function __Construct()
    {
        $this->db= \Config\Database::connect();
        $this->builder=$this->db->table($this->tbl);
    }
    function getAlldata()
    {
        return $this->builder->get();
    }
}