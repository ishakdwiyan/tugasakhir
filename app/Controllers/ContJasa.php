<?php
namespace App\Controllers;

class ContJasa extends BaseController
{
    function index()
    {
        $halaman['dataJasa']= $this->objJasa->getAlldatajoincat();

        $halaman['admpage']='jasa-view';
        return view ("backend", $halaman);
    }
    function input_data()
    {
        $data['datakategori']=$this->objKat->getAlldata();

        $data['admpage']="jasa-form";
        return view ("backend", $data);
    }
    function submit_form($idJasa=false)
    {
		if($idJasa!=false)
		{
			$paramJasa=array('idJasa'=>$idJasa);
			$rec=$this->objJasa->DataBy($paramJasa)->getRow();

			$data['idJasa'] 			= $rec->idJasa;
			$data['namajasa'] 			= $rec->namajasa;
			$data['slugJasa'] 			= $rec->slugJasa;
			$data['hargaAnggota'] 		= $rec->hargaAnggota;
			$data['hargaNonAnggota'] 	= $rec->hargaNonAnggota;
			$data['hargaWnaCampur'] 	= $rec->hargaWnaCampur;
			$data['hargaWnaAsli'] 		= $rec->hargaWnaAsli;
			$data['foto'] 				= $rec->foto;
			$data['seotitle'] 			= $rec->seotitle;
			$data['seodesc'] 			= $rec->seodesc;
			$data['alt'] 				= $rec->alt;
		}



        // 1. Memeriksa apakah proses pengiriman data dari formulir dengan menekan tombol submit

		if($this->request->getMethod()=='post')
		{
			//2. Melakukan Validasi elemen form pada formulir

			// 2.1 Menentukan aturan pada setiap elemen form
			$rules=[
				'namajasa'=>[
					'label' => 'Nama Jasa',
					'rules'	=> 'required|max_length[150]',
					'errors'=>[
						'required'   => 'Nama Jasa tidak boleh kosong',
						'max_length' => 'Panjang karakter nama Jasa maksimal 20 karakter'
					]
				]
			];
            
            // 2.2 Memvalidasi/memeriksa aturan yang sudah dibuat

			if($this->validate($rules))
			{
                // 2.3 Upload Foto

				$path='./assets/image/jasa/'; // posisi foto ukuran besar

				// Load library image untuk membuat foto menjadi ukuran kecil
				$images=service('image'); // library untuk manipulasi foto

				// menangkap semua file yang diupload pada elemen form benama uploadfile

				$files=$this->request->getFiles('uploadfile');
				$totalFile=count($files['uploadfile']);
				$noFile=1;
                
                foreach($files['uploadfile'] as $file)
				{
					if($noFile==1)
					{
						if($file->isValid() && !$file->hasMoved())
						{
							// Perintah untuk upload file
							$file->move('./assets/image/jasa', $file->getRandomName());

							// mendapatkan nama foto yang sudah dipload
							$fileName=$file->getName();

							// tahapan pembuatan foto thumbs
							// Bisa dihapus jika tidak diperlukan foto kecil
							$images->withFile($path.$fileName)
							->fit(150,150,'center')
							->save('./assets/image/jasaThumb/'.$fileName);
                        }
                        else
                        {
                           echo 'gagal';
                        }
                    }
                }

                // 2.4 Mempersiapkan array nilai yang mau disimpan ke tabel

				// Slug Route

				$txtslug	= trim($this->request->getPost('slugJasa'));
				$linkslug	= str_replace(' ', '-',$txtslug);
				$newslug	= strtolower($linkslug);
				
				$data_save=array(
					'idJasa'		    => $this->request->getPost('idJasa'),
					'namajasa'		    => $this->request->getPost('namajasa'),
					// 'slugProduk'		=> $newslug,
					'slugJasa'			=> $this->request->getPost('slugJasa'),
					'hargaAnggota'		=> $this->request->getPost('hargaAnggota'),
					'hargaNonAnggota'	=> $this->request->getPost('hargaNonAnggota'),
					// 'fotoProduk'		=> $fileName,
					'hargaWnaCampur'	=> $this->request->getPost('hargaWnaCampur'),
					'hargaWnaAsli'		=> $this->request->getPost('hargaWnaAsli'),
					'idkategori'		=> $this->request->getPost('idkategori'),
					'keterangan'		=> $this->request->getPost('keterangan'),
					'foto'	            => $this->request->getPost('foto'),
				);

				// Memanggil function pada model untuk menyimpan data

				$idJasa=$this->objJasa->save_data($data_save);

            }
            else
            {
                $data['validation']=$this->validator;
				$data['datakategori']=$this->objKat->getAlldata();
				$data['admpage']='jasa-form';
				return view("backend",$data);
            }
        }
        else
        {
            $data['datakategori']=$this->objKat->getAlldata();
			$data['admpage']='jasa-form';
			return view("backend",$data);
        }
        
    }
    
	function delete($idJasa)
    {
        echo $idJasa;
    }
}