<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'ContBase::index');
$routes->get('/product', 'ContBase::data_product');

//Admin Route

$routes->get('/dashboard', 'ContAdm::index');
$routes->get('/dashboard/data-jasa', 'ContJasa::index');
$routes->get('/dashboard/data-jasa/tambah', 'ContJasa::input_data');
$routes->get('dashboard/data-jasa/ubah/(:num)', 'ContJasa::submit_form/$1');
$routes->get('/dashboard/data-jasa/delete/(:num)', 'ContJasa::delete/$1');
$routes->post('dashboard/data-jasa/simpan','ContJasa::submit_form');
